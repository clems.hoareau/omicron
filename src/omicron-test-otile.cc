/**
 * @file 
 * @brief Program to test the Otile class.
 *
 * @author Florent Robinet - <a href="mailto:florent.robinet@ijclab.in2p3.fr">florent.robinet@ijclab.in2p3.fr</a>
 */
#include "Otile.h"

using namespace std;

/**
 * @brief Test program.
 */
int main (int argc, char* argv[]){

  //*********************************************************************
  // Omap
  //*********************************************************************
  cout<<"\n***** Omap *****"<<endl;
  //                    Q    fs   fmi    fma   T   mm
  Omap *O = new Omap(4.66, 2048, 20.0, 500.0, 64, 0.2);
  cout<<"\tGetQ()"<<endl;
  if(O->GetQ()!=4.66) return 1;
  cout<<"\tGetTimeMin()"<<endl;
  if(O->GetTimeMin()!=-32.0) return 1;
  cout<<"\tGetTimeMax()"<<endl;
  if(O->GetTimeMax()!=32.0) return 1;
  cout<<"\tGetTimeRange()"<<endl;
  if(O->GetTimeRange()!=64.0) return 1;
  cout<<"\tGetFrequencyMin()"<<endl;
  if(O->GetFrequencyMin()!=20.0) return 1;
  cout<<"\tGetBandN()"<<endl;
  if(O->GetBandN()!=16.0) return 1;
  cout<<"\tGetTileN()"<<endl;
  if(O->GetTileN()!=585728) return 1;
  cout<<"\tGetBandIndex() 1"<<endl;
  if(O->GetBandIndex(21)!=0) return 1;
  cout<<"\tGetBandIndex() 2"<<endl;
  if(O->GetBandIndex(20)!=0) return 1;
  cout<<"\tGetBandIndex() 3"<<endl;
  if(O->GetBandIndex(10)!=-1) return 1;
  cout<<"\tGetBandIndex() 4"<<endl;
  if(O->GetBandIndex(500)!=16) return 1;
  cout<<"\tGetBandIndex() 5"<<endl;
  if(O->GetBandIndex(30)!=2) return 1;
  cout<<"\tGetBandFrequency()"<<endl;
  if(O->GetBandFrequency(3)<40 || O->GetBandFrequency(3)>41) return 1;
  cout<<"\tGetBandStart()"<<endl;
  if(O->GetBandStart(0)!=O->GetFrequencyMin()) return 1;
  cout<<"\tGetBandEnd()"<<endl;
  if(O->GetBandEnd(15)!=O->GetFrequencyMax()) return 1;
  cout<<"\tGetBandTileN()"<<endl;
  if(O->GetBandTileN(0)!=4096) return 1;
  cout<<"\tGetTileContent()"<<endl;
  O->SetTileContent(123, 0, 1234.5);
  if(O->GetTileContent(123, 0)!=1234.5) return 1;
  if(O->GetTileContent(124, 0)!=0.0) return 1;
  cout<<"\tApplyOffset()"<<endl;
  O->ApplyOffset(12.0);
  if(O->GetTimeMin()!=-20.0) return 1;
  if(O->GetTimeMax()!=44.0) return 1;
  O->ApplyOffset(-12.0);
  if(O->GetTimeMin()!=-32.0) return 1;
  if(O->GetTimeMax()!=32.0) return 1;

  delete O;

  //*********************************************************************
  // Oqplane
  //*********************************************************************
  cout<<"\n***** Oqplane *****"<<endl;
  //                       Q    fs   fmi    fma   T   mm
  Oqplane *Q = new Oqplane(4.66, 2048, 20.0, 500.0, 64, 0.2);
  cout<<"\tSetSnrThr()"<<endl;
  Q->SetSnrThr(123.4);
  if(Q->GetSnrThr()!=123.4) return 2;
  cout<<"\tGetTileSnrSq()"<<endl;
  if(Q->GetTileSnrSq(123, 0)!=0) return 2;
  cout<<"\tGetTileAmplitude()"<<endl;
  if(Q->GetTileAmplitude(123, 0)!=0) return 2;

  delete Q;
  
  //*********************************************************************
  // Otile
  //*********************************************************************
  cout<<"\n***** Otile *****"<<endl;
  Otile *T = new Otile(64, 3.31662, 100, 20.0, 500.0, 2048, 0.2);
  cout<<"\tGetQN()"<<endl;
  if(T->GetQN()!=5) return 3;
  cout<<"\tGetPlotTimeWindows()"<<endl;
  vector <unsigned int> win;
  win.push_back(1); win.push_back(4); win.push_back(9); 
  T->SetPlotTimeWindows(win);
  vector <unsigned int> win2 = T->GetPlotTimeWindows();
  if(win2.size()!=3) return 3;
  if(win2[01]!=4) return 3;

  Segments *S = new Segments(1300000000, 1300000200);
  S->AddSegment(1300001000, 1300001060);
  S->AddSegment(1300002000, 1300002064);
  T->SetSegments(S);
  T->SetOverlapDuration(4);
  cout<<"\tGetOverlapDuration()"<<endl;
  if(T->GetOverlapDuration()!=4) return 3;
  delete S;
  bool newseg = false;
  cout<<"\tNewChunk()"<<endl;
  if(T->NewChunk(newseg)!=true) return 3;
  if(newseg!=true) return 3;
  cout<<"\tGetChunkTimeStart()"<<endl;
  if(T->GetChunkTimeStart()!=1300000000) return 3;
  cout<<"\tGetChunkTimeCenter()"<<endl;
  if(T->GetChunkTimeCenter()!=1300000032) return 3;
  cout<<"\tGetChunkTimeEnd()"<<endl;
  if(T->GetChunkTimeEnd()!=1300000064) return 3;
  cout<<"\tGetCurrentOverlapDuration()"<<endl;
  if(T->GetCurrentOverlapDuration()!=4) return 3;
  cout<<"\tNewChunk()"<<endl;
  if(T->NewChunk(newseg)!=true) return 3;
  if(newseg!=false) return 3;
  cout<<"\tGetChunkTimeStart()"<<endl;
  if(T->GetChunkTimeStart()!=1300000060) return 3;
  cout<<"\tGetChunkTimeCenter()"<<endl;
  if(T->GetChunkTimeCenter()!=1300000092) return 3;
  cout<<"\tGetChunkTimeEnd()"<<endl;
  if(T->GetChunkTimeEnd()!=1300000124) return 3;
  cout<<"\tGetCurrentOverlapDuration()"<<endl;
  if(T->GetCurrentOverlapDuration()!=4) return 3;
  cout<<"\tNewChunk()"<<endl;
  if(T->NewChunk(newseg)!=true) return 3;
  if(newseg!=false) return 3;
  cout<<"\tGetChunkTimeStart()"<<endl;
  if(T->GetChunkTimeStart()!=1300000120) return 3;
  cout<<"\tGetChunkTimeCenter()"<<endl;
  if(T->GetChunkTimeCenter()!=1300000152) return 3;
  cout<<"\tGetChunkTimeEnd()"<<endl;
  if(T->GetChunkTimeEnd()!=1300000184) return 3;
  cout<<"\tGetCurrentOverlapDuration()"<<endl;
  if(T->GetCurrentOverlapDuration()!=4) return 3;
  cout<<"\tNewChunk()"<<endl;
  if(T->NewChunk(newseg)!=true) return 3;
  if(newseg!=false) return 3;
  cout<<"\tGetChunkTimeStart()"<<endl;
  if(T->GetChunkTimeStart()!=1300000136) return 3;
  cout<<"\tGetChunkTimeCenter()"<<endl;
  if(T->GetChunkTimeCenter()!=1300000168) return 3;
  cout<<"\tGetChunkTimeEnd()"<<endl;
  if(T->GetChunkTimeEnd()!=1300000200) return 3;
  cout<<"\tGetCurrentOverlapDuration()"<<endl;
  if(T->GetCurrentOverlapDuration()!=48) return 3;
  cout<<"\tNewChunk()"<<endl;
  if(T->NewChunk(newseg)!=true) return 3;
  if(newseg!=true) return 3;
  cout<<"\tGetChunkTimeStart()"<<endl;
  if(T->GetChunkTimeStart()!=1300002000) return 3;
  cout<<"\tGetChunkTimeCenter()"<<endl;
  if(T->GetChunkTimeCenter()!=1300002032) return 3;
  cout<<"\tGetChunkTimeEnd()"<<endl;
  if(T->GetChunkTimeEnd()!=1300002064) return 3;
  cout<<"\tGetCurrentOverlapDuration()"<<endl;
  if(T->GetCurrentOverlapDuration()!=4) return 3;
  if(T->NewChunk(newseg)!=false) return 3;


  delete T;
  return 0;
}

